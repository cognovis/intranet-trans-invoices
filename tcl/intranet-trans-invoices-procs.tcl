# /packages/intranet-trans-invoices/tcl/intranet-trans-invoices-procs.tcl

ad_library {
    Bring together all "components" (=HTML + SQL code)
    related to Invoices

    @author frank.bergmann@project-open.com
    @creation-date  27 June 2003
}


# ------------------------------------------------------
# Price List
# ------------------------------------------------------

ad_proc im_trans_price_component { user_id company_id return_url} {
    Returns a formatted HTML table representing the
    prices for the current company
} {
    if {![im_permission $user_id view_costs]} { return "" }
    
    set enable_file_type_p [parameter::get_from_package_key -package_key intranet-trans-invoices -parameter "EnableFileTypeInTranslationPriceList" -default 0]

    set bgcolor(0) " class=roweven "
    set bgcolor(1) " class=rowodd "
    set price_format "000.000"
    set min_price_format "000.00"
    set price_url_base "/intranet-trans-invoices/price-lists/new"

    set colspan 9
    if {$enable_file_type_p} { incr colspan}

    set file_type_html "<td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.File_Type "File Type"]</td>"
    if {!$enable_file_type_p} { set file_type_html "" }

    set price_list_html "
    	<form action=/intranet-trans-invoices/price-lists/price-action method=POST>
	[export_form_vars company_id return_url]
	<table border=0>
	<tr><td colspan=$colspan class=rowtitle align=center>[_ intranet-trans-invoices.Price_List]</td></tr>
	<tr class=rowtitle>
		  <td class=rowtitle>[_ intranet-trans-invoices.UoM]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Task_Type]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Source]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Target]</td>
		  <td class=rowtitle>[_ intranet-trans-invoices.Subject]</td>
		  $file_type_html
		  <td class=rowtitle>[_ intranet-trans-invoices.Rate]</td>
		  <td class=rowtitle>[lang::message::lookup "" intranet-trans-invoices.Minimum_Rate "Min Rate"]</td>
		  <td class=rowtitle>[_ intranet-core.Note]</td>
		  <td class=rowtitle>[im_gif -translate_p 1 del "Delete"]</td>
	</tr>
    "

    set price_rows_html ""
    set ctr 1
    set old_currency ""
    db_foreach prices {

select
	p.*,
	c.company_path as company_short_name,
	im_category_from_id(uom_id) as uom,
	im_category_from_id(task_type_id) as task_type,
	im_category_from_id(target_language_id) as target_language,
	im_category_from_id(source_language_id) as source_language,
	im_category_from_id(subject_area_id) as subject_area,
	im_category_from_id(file_type_id) as file_type,
	to_char(min_price, :min_price_format) as min_price_formatted
from
	im_trans_prices p
      LEFT JOIN
	im_companies c USING (company_id)
where
	p.company_id=:company_id
order by
	im_category_from_id(task_type_id) desc,
	im_category_from_id(subject_area_id) desc,
	im_category_from_id(source_language_id) desc,
	im_category_from_id(target_language_id) desc,
	currency,
	uom_id
} {

        # There can be errors when formatting an empty string...
        set price_formatted ""
        catch { set price_formatted "[format "%0.3f" $price] $currency" } errmsg
	if {"" != $old_currency && ![string equal $old_currency $currency]} {
	    append price_rows_html "<tr><td colspan=$colspan>&nbsp;</td></tr>\n"
	}

        set price_url [export_vars -base $price_url_base { company_id price_id return_url }]

	set file_type_html "<td>$file_type</td>"
	if {!$enable_file_type_p} { set file_type_html "" }

	append price_rows_html "
        <tr $bgcolor([expr $ctr % 2]) nobreak>
	  <td>$uom</td>
	  <td>$task_type</td>
	  <td>$source_language</td>
          <td>$target_language</td>
	  <td>$subject_area</td>
	  $file_type_html
          <td><a href=\"$price_url\">$price_formatted</a></td>
          <td>$min_price_formatted</td>
          <td>[string_truncate -len 15 $note]</td>
          <td><input type=checkbox name=price_id.$price_id></td>
	</tr>"
	incr ctr
	set old_currency $currency
    }

    if {$price_rows_html != ""} {
	append price_list_html $price_rows_html
    } else {
	append price_list_html "<tr><td colspan=$colspan align=center><i>[_ intranet-trans-invoices.No_prices_found]</i></td></tr>\n"
    }

    set sample_pracelist_link "<a href=/intranet-trans-invoices/price-lists/pricelist_sample.csv>[_ intranet-trans-invoices.lt_sample_pricelist_CSV_]</A>"

    if {[im_permission $user_id add_costs]} {
        append price_list_html "
	<tr>
	  <td colspan=$colspan align=right>
	    <input type=submit name=add_new value=\"[_ intranet-trans-invoices.Add_New]\">
	    <input type=submit name=del value=\"[_ intranet-trans-invoices.Del]\">
	  </td>
	</tr>
        "
    }

    append price_list_html "
       </table>
       </form>
    "

    if {[im_permission $user_id add_costs]} {
        append price_list_html "
	<ul>
	  <li>
	    <a href=/intranet-trans-invoices/price-lists/upload-prices?[export_url_vars company_id return_url]>
	      [_ intranet-trans-invoices.Upload_prices]</A>
	    [_ intranet-trans-invoices.lt_for_this_company_via_]
	  <li>
	    [_ intranet-trans-invoices.lt_Check_this_sample_pra]
	    [_ intranet-trans-invoices.lt_It_contains_some_comm]
	  <li>
	    <a href=\"[export_vars -base "/intranet-reporting/view" {{report_code translation_price_list_export} {format csv} company_id}]\">
	    [lang::message::lookup "" intranet-trans-invoices.Export_as_csv "Export price list as CSV"]
	    </a>
	</ul>
        "
    }

    return $price_list_html
}


# ---------------------------------------------------------------
# Permissions
# ---------------------------------------------------------------

ad_proc -public im_trans_invoice_permissions {
    {-debug 0}
    current_user_id
    invoice_id
    view_var
    read_var
    write_var
    admin_var
} {
    Fill the "by-reference" variables read, write and admin
    with the permissions of $current_user_id on $invoice_id
} {
    upvar $view_var view
    upvar $read_var read
    upvar $write_var write
    upvar $admin_var admin

	set perm_proc [ad_parameter -package_id [im_package_invoices_id] "InvoicePermissionProc"]
	
	$perm_proc $current_user_id $invoice_id view read write admin

    if {$debug} { ns_log Notice "im_trans_invoice_permissions: cur=$current_user_id, user=$user_id, view=$view, read=$read, write=$write, admin=$admin" }

}

ad_proc -public im_trans_invoice_create_from_tasks {
    {-cost_type_id ""}
    {-invoice_id ""}
    {-task_ids ""}
    {-cost_center_id ""}
    -project_id
} {
    Create a quote / invoice from the tasks. This is CUSTOMER Facing and uses Customer Pricing.
    
    @param project_id Project ID for which to create the translation invoice
    @param invoice_id Invoice ID we want to UPDATE. In this case the line items will be removed and new line items generated for the tasks in the project
    @param task_ids List of TaskIDs which will be used to limit the invoice to only include these tasks. Helpful if you want to create a partial invoice
    @param cost_center_id Cost center to use
    @param cost_type_id Cost Type for the invoice. Defaults to quote, but could also be an actual customer invoice.
} {
    if {$cost_type_id eq ""} {set cost_type_id [im_cost_type_quote]}
    
    # Get info about the project
    db_1row project_info "select company_id,project_lead_id,subject_area_id,project_cost_center_id from im_projects where project_id = :project_id"
    
    if {$cost_center_id eq ""} {
	set cost_center_id $project_cost_center_id
    }
    
    set num_tasks [db_string tasks "select count(task_id) from im_trans_tasks where project_id = :project_id" -default 0]
    
    if {$num_tasks eq 0} {
	return
	ad_script_abort
    }
    
    # ---------------------------------------------------------------
    # Create the Quote
    # ---------------------------------------------------------------
    
    # Get the payment days from the company
    set payment_term_id [db_string default_payment_days "select payment_term_id from im_companies where company_id = :company_id" -default ""]
    set payment_days ""
    if {"" != $payment_term_id} {
	set payment_days [db_string payment_days "select aux_int1 from im_categories where category_id = :payment_term_id" -default ""]
    }
    if {$payment_days == ""} {
	set payment_days [ad_parameter -package_id [im_package_cost_id] "DefaultCompanyInvoicePaymentDays" "" 30]
    }
    
    set provider_id [im_company_internal]
	
    # get company information
    db_1row company_info "select
		c.*,
		o.*,
		cc.country_name
	from
		im_companies c
	  	LEFT JOIN im_offices o ON c.main_office_id=o.office_id
	  	LEFT JOIN country_codes cc ON o.address_country_code=cc.iso
	where
		c.company_id = :company_id"
	
    set company_contact_id [im_invoices_default_company_contact $company_id $project_id]

    # First we check for project company_office_id
    set invoice_office_id [db_string company_accounting_office "select company_office_id from im_projects where project_id = :project_id" -default ""]

    # If that 'company_office_id' in 'im_projects' is null, we use main_office_id as value
    if {$invoice_office_id eq ""}  {
        set invoice_office_id [db_string company_main_office_info "select main_office_id from im_companies where company_id = :company_id" -default ""]
    }
    # Get some defaults if missing from the internal company
    if {$default_quote_template_id eq ""} {
	set default_quote_template_id [db_string template "select default_quote_template_id from im_companies where company_id = [im_company_internal]" -default ""]
    }
	
    set invoice_date [db_string get_today "select now()::date"]
    
    if {$invoice_id eq ""} {
	# Create the invoice
	set cost_status_id [im_cost_status_created]
	set invoice_id [im_new_object_id]
	set invoice_nr [im_next_invoice_nr -cost_type_id $cost_type_id -cost_center_id $cost_center_id]
	
	# ---------------------------------------------------------------
	# GET THE CORRECT VAT (MISSING)
	# ---------------------------------------------------------------
	
	db_exec_plsql create_invoice {
	    select im_trans_invoice__new (
					  :invoice_id,
					  'im_trans_invoice',
					  now(),
					  :project_lead_id,
					  '0.0.0.0',
					  null,
					  :invoice_nr,
					  :company_id,
					  :provider_id,
					  :company_contact_id,
					  :invoice_date,
					  'EUR',
					  :default_quote_template_id,
					  :cost_status_id,
					  :cost_type_id,
					  :default_payment_method_id,
					  :payment_days,
					  '0',
					  :default_vat,
					  :default_tax,
					  null
			);
	}   
	    # Not sure about this one, as invoice_office_id was here before, but was never used during creation of invoice
	    if {$invoice_id ne ""} {
	       db_dml update_invoice_office_id "update im_invoices set invoice_office_id = :invoice_office_id where invoice_id =:invoice_id"
        }

    } else {
	db_1row invoice_info "select invoice_nr, cost_status_id from im_invoices,im_costs where invoice_id = cost_id and invoice_id = :invoice_id"
	
	# invoice exists, remove all line items
	db_dml delete_invoice_items "
			DELETE from im_invoice_items
			WHERE invoice_id=:invoice_id
		"
	# Update the effective date
	db_dml update_date "update im_costs set effective_date = :invoice_date where cost_id = :invoice_id"
    }
    
    # Update cost_center
    db_dml update_cost_center "update im_costs set cost_center_id = :cost_center_id where cost_id = :invoice_id"
    
    # ---------------------------------------------------------------
    # Now add the tasks as line items to the invoice
    # ---------------------------------------------------------------
    set sort_order 1
    set currency [ad_parameter -package_id [im_package_cost_id] "DefaultCurrency" "" "EUR"]
    
    # Check if we have languages with the minimum rate.
    set min_price_languages [list]
    db_foreach tasks {
	select sum(billable_units) as sum_tasks, array_to_string(array_agg(task_name), ',') as task_name_agg, 
	target_language_id, task_uom_id, task_type_id, source_language_id, im_file_type_from_trans_task(task_id) as file_type_id 
	from im_trans_tasks 
	where project_id = :project_id 
	group by target_language_id, task_uom_id, task_type_id, source_language_id, file_type_id
    } {
	set rate_info [im_trans_invoice_rate \
			   -company_id $company_id \
			   -task_type_id $task_type_id \
			   -subject_area_id $subject_area_id \
			   -target_language_id $target_language_id \
			   -source_language_id $source_language_id \
			   -currency $currency \
			   -task_uom_id $task_uom_id \
			   -task_sum $sum_tasks
		      ]
	
	# Split the rate information up into the components
	set billable_units [lindex $rate_info 0]
	set rate_uom_id [lindex $rate_info 1]
	set rate [lindex $rate_info 2]
	
	# We found a language where the tasks together are below the
	# Minimum Price.
	if {[im_uom_unit] eq $rate_uom_id && [im_uom_unit] ne $task_uom_id} {
	    lappend min_price_languages $target_language_id
	    
	    set material_id [im_material_create_from_parameters -material_uom_id $task_uom_id -debug 1]
	    
	    # Insert the min price
	    
	    set item_id [db_nextval "im_invoice_items_seq"]
	    set task_list [split $task_name_agg ","]
	    if {[llength $task_list] >1} {
		set task_name " [join $task_list "<br>"]"
	    } else {
		set task_name [lindex $task_list 0]
	    }
	    
	    set insert_invoice_items_sql "
			INSERT INTO im_invoice_items (
				item_id, item_name,
				project_id, invoice_id,
				item_units, item_uom_id, item_material_id,
				price_per_unit, currency,
				sort_order, item_type_id,
				item_status_id, description, task_id
			) VALUES (
				:item_id, :task_name,
				:project_id, :invoice_id,
				:billable_units, :rate_uom_id, :material_id,
				:rate, :currency,
				:sort_order, :task_type_id,
				null, '', null
			)"
	    db_dml insert_min_price_items $insert_invoice_items_sql
		
	    incr sort_order
	}
    }
    
    set task_sql "select task_type_id,
		target_language_id,
		source_language_id,
		task_uom_id,
		task_name,
		billable_units,
		task_id,
		im_file_type_from_trans_task(task_id) as file_type_id
	from im_trans_tasks where project_id = :project_id"
    
    if {$task_ids ne ""} {
	append task_sql " and task_id in ([template::util::tcl_to_sql_list $task_ids])"
    }
    
    if {$task_ids ne ""} {
	append task_sql " and task_id in ([template::util::tcl_to_sql_list $task_ids])"
    }
    
    # Order by filename and language
    append task_sql " order by task_name, im_name_from_id(target_language_id)"
    db_foreach tasks $task_sql {
	
	if {[lsearch $min_price_languages $target_language_id]<0 } {
	    set material_id [im_material_create_from_parameters -material_uom_id $task_uom_id -debug 1]
	    
	    # Ignore the minimum rate
	    set rate_info [im_trans_invoice_rate \
			       -company_id $company_id \
			       -task_type_id $task_type_id \
			       -subject_area_id $subject_area_id \
			       -target_language_id $target_language_id \
			       -source_language_id $source_language_id \
			       -currency $currency \
			       -task_uom_id $task_uom_id \
			       -file_type_id $file_type_id \
			       -task_sum $billable_units \
			       -ignore_min_price
			  ]
			
	    # Split the rate information up into the components
	    set billable_units [lindex $rate_info 0]
	    set task_uom_id [lindex $rate_info 1]
	    set rate [lindex $rate_info 2]
	    
	    set item_id [db_nextval "im_invoice_items_seq"]
	    
	    set insert_invoice_items_sql "
			INSERT INTO im_invoice_items (
				item_id, item_name,
				project_id, invoice_id,
				item_units, item_uom_id, item_material_id,
				price_per_unit, currency,
				sort_order, item_type_id,
				item_status_id, description, task_id
			) VALUES (
				:item_id, :task_name,
				:project_id, :invoice_id,
				:billable_units, :task_uom_id, :material_id,
				:rate, :currency,
				:sort_order, :task_type_id,
				null, '', :task_id
			)"
	    db_dml insert_invoice_items $insert_invoice_items_sql
	    incr sort_order
	}
    }
    
    # Update the invoice amount based on the invoice items
    im_invoice_update_rounded_amount -invoice_id $invoice_id
    
    
    db_dml update_cost "update im_costs set project_id = :project_id where cost_id = :invoice_id"
    
    db_1row "get relations" "
        			select  count(*) as v_rel_exists
        			from    acs_rels
        			where   object_id_one = :project_id
        					and object_id_two = :invoice_id
        "
    if {0 ==  $v_rel_exists} {
    	set rel_id [db_exec_plsql create_invoice_rel "	  select acs_rel__new (
      	   null,             -- rel_id
      	   'relationship',   -- rel_type
      	   :project_id,      -- object_id_one
      	   :invoice_id,      -- object_id_two
      	   null,             -- context_id
      	   null,             -- creation_user
      	   null             -- creation_ip
      )"]
    }
    
	
    db_release_unused_handles
    
    # Audit creation
    im_audit -object_type "im_invoice" -object_id $invoice_id -action after_create -status_id $cost_status_id -type_id $cost_type_id
    
    return $invoice_id
}


ad_proc -public im_trans_invoice_rate {
	-company_id
	-task_type_id
	{-subject_area_id ""}
	-target_language_id
	-source_language_id
	{-file_type_id ""}
	{-currency "EUR"}
	-task_uom_id
	-task_sum
	-ignore_min_price:boolean
} {
	Calculate the best rate for a customer.

	Supports looping up through the prices for parent categories
} {
	set rate_info [im_translation_best_match_price \
		-company_id $company_id \
		-task_type_id $task_type_id \
		-subject_area_id $subject_area_id \
		-target_language_id $target_language_id \
		-source_language_id $source_language_id \
		-invoice_currency $currency \
		-task_uom_id $task_uom_id \
		-file_type_id $file_type_id \
		-task_sum $task_sum \
		-ignore_min_price_p $ignore_min_price_p
	]

	set rate [lindex $rate_info 2]
	if {$rate eq 0 || $rate eq ""} {
		# No rate found, try with the parent languages
	   	set parent_source_language_id [lindex [im_category_parents $source_language_id] 0]
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $subject_area_id \
			-target_language_id $target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-task_sum $task_sum \
	 		-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}

	if {$rate eq 0 || $rate eq ""} {
		# Still no file found, try with the parent_source and subject_area
		set parent_subject_area_id [lindex [im_category_parents $subject_area_id] 0]
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $parent_subject_area_id \
			-target_language_id $target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-task_sum $task_sum \
		 	-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}

	if {$rate eq 0 || $rate eq ""} {
		# Still no file found, try with the parent_target
		set parent_target_language_id [lindex [im_category_parents $target_language_id] 0]
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $subject_area_id \
			-target_language_id $parent_target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-task_sum $task_sum \
		 	-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}

	if {$rate eq 0 || $rate eq ""} {
		# Still no file found, try with all the parents
		set rate_info [im_translation_best_match_price \
			-company_id $company_id \
			-task_type_id $task_type_id \
			-subject_area_id $parent_subject_area_id \
			-target_language_id $parent_target_language_id \
			-source_language_id $parent_source_language_id \
			-invoice_currency $currency \
			-task_uom_id $task_uom_id \
			-file_type_id $file_type_id \
			-task_sum $task_sum \
			-ignore_min_price_p $ignore_min_price_p
		]
		set rate [lindex $rate_info 2]
	}
	return $rate_info
}

